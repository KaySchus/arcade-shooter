var game = new Phaser.Game(800, 600, Phaser.AUTO, "game");

game.state.add("Boot", boot);
game.state.add("Initialize", initialize);
game.state.add("MainMenu", mainMenu);
game.state.add("GameState", gameState);

game.state.start("Boot");