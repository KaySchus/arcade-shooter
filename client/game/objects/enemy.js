Enemy = function(game, x, y) {
	Phaser.Sprite.call(this, game, x, y, "alien");

	this.texture.baseTexture.scaleMode = PIXI.scaleModes.NEAREST;
	this.anchor.set(0.5, 0.5);
};

Enemy.prototype = Object.create(Phaser.Sprite.prototype);
Enemy.prototype.constructor = Enemy;

Enemy.prototype.update = function(node) {
};

PatternEnemy = function(game, pattern, offset) {
	Phaser.Sprite.call(this, game, 0, 0, "alien");

	this.texture.baseTexture.scaleMode = PIXI.scaleModes.NEAREST;
	this.anchor.set(0.5, 0.5);

	this.pattern = pattern;
	this.step = 0;

	this.weapon = null;
	this.stepsPerFire = 50;

	this.offset = offset;

	var node = this.pattern.getStepCoordinates(this.step);
	this.x = node.x;
	this.y = node.y + this.offset;
}

PatternEnemy.prototype = Object.create(Phaser.Sprite.prototype);
PatternEnemy.prototype.constructor = PatternEnemy;

PatternEnemy.prototype.attachWeapon = function(weapon) {
	this.weapon = weapon;
}

PatternEnemy.prototype.update = function() {
	this.step++;

	if (this.step === this.pattern.path.length) {
		this.step = 0;
	}

	var node = this.pattern.getStepCoordinates(this.step);
	this.x = node.x;
	this.y = node.y + this.offset;

	if (this.weapon) {
		if (this.step > this.stepsPerFire) {
			var chance = this.game.rnd.between(0, 100);
			if (chance === 1) {
				this.weapon.fire(this);
			}
		}
	}
};

Pattern = { }

Pattern.Linear = function(game) {
	this.points = {
		'x': [ 800, 700, 600, 500, 400, 300, 200, 100,   0 ],
		'y': [   0,   0,   0,   0,   0,   0,   0,   0,   0 ]
	};

	this.pi = 0;
	this.path = [];

	var ix = 0;
	var x = 1 / game.width;

	for (var i = 0; i <= 1; i += x) {
		var px = Phaser.Math.linearInterpolation(this.points.x, i);
		var py = Phaser.Math.linearInterpolation(this.points.y, i);

		var node = { x: px, y: py, angle: 0 };

		if (ix > 0) {
			node.angle = Phaser.Math.angleBetweenPoints(this.path[ix - 1], node);
		}

		this.path.push(node);

		ix++;
	}
};

Pattern.Linear.prototype.getStepCoordinates = function(step) {
	if (step < 0 || step >= this.path.length) {
		return null;
	}

	return this.path[step];
};

Pattern.CrazyWave = function(game) {
	this.points = {
		'x': [ 800, 700, 600, 500, 400, 300, 200, 100,   0 ],
		'y': [ 100, 300, 300,   0,   0, 300, 300, 100, 100 ]
	};

	this.pi = 0;
	this.path = [];

	var ix = 0;
	var x = 1 / game.width;

	for (var i = 0; i <= 1; i += x) {
		var px = Phaser.Math.bezierInterpolation(this.points.x, i);
		var py = Phaser.Math.bezierInterpolation(this.points.y, i);

		var node = { x: px, y: py, angle: 0 };

		if (ix > 0) {
			node.angle = Phaser.Math.angleBetweenPoints(this.path[ix - 1], node);
		}

		this.path.push(node);

		ix++;
	}
};

Pattern.CrazyWave.prototype.getStepCoordinates = function(step) {
	if (step < 0 || step >= this.path.length) {
		return null;
	}

	return this.path[step];
};