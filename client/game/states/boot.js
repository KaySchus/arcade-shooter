var boot = function(game) {
	console.log("%cArcade Shooter v0.1 is loading.", "color:white; background:red");
}

boot.prototype = {
	preload: function() {
		this.game.load.image("loading", "assets/loading.png");

		this.game.stage.scale.pageAlignHorizontally = true;
		this.game.stage.scale.pageAlignVeritcally = true;
	},

	create: function() {
		this.game.state.start("Initialize");  
	}
}