var gameState = function(game) {
	 this.players = null;
	 this.player = null;

	 this.cursors = null;
	 this.wasd = null;

	 this.weapons = [];
	 this.currentWeapon = 0;
	 this.weaponName = null;

	 this.style = null;
	 this.score = 0;
	 this.scoreText = null;

	 this.enemies = null;
	 this.patterns = [];

	 this.emitter = null;
}

gameState.prototype = {
	create: function() {
		this.game.physics.startSystem(Phaser.Physics.ARCADE);

		this.game.add.sprite(0, 0, 'background');

		this.players = this.game.add.group();
		this.players.enableBody = true;

		this.enemies = this.game.add.group();
		this.enemies.enableBody = true;

		this.player = new Player(this.game, 32, this.game.world.height - 150);
		this.players.add(this.player);
		this.player.body.collideWorldBounds = true;

		this.cursors = this.game.input.keyboard.createCursorKeys();

		this.wasd = {
			up: this.game.input.keyboard.addKey(Phaser.Keyboard.W),
			down: this.game.input.keyboard.addKey(Phaser.Keyboard.S),
			left: this.game.input.keyboard.addKey(Phaser.Keyboard.A),
			right: this.game.input.keyboard.addKey(Phaser.Keyboard.D),
		};

		this.weapons.push(new Weapon.SingleBullet(this.game, "Single Bullet"));
		this.weapons.push(new Weapon.FrontAndBack(this.game));
		this.weapons.push(new Weapon.ThreeWay(this.game));
		this.weapons.push(new Weapon.EightWay(this.game));
		this.weapons.push(new Weapon.ScatterShot(this.game));
		this.weapons.push(new Weapon.Beam(this.game));
		this.weapons.push(new Weapon.SplitShot(this.game));
		this.weapons.push(new Weapon.Pattern(this.game));
		this.weapons.push(new Weapon.Shotgun(this.game));

		this.patterns.push(new Pattern.Linear(this.game));
		this.patterns.push(new Pattern.CrazyWave(this.game));

		this.currentWeapon = 0;

		this.weaponName = this.add.bitmapText(0, 0, "shmupfont", this.weapons[this.currentWeapon].name);

		this.style = { font: "20px Courier", full: "#fff" };
		this.scoreText = this.game.add.text(500, 20, "Score: 0");

		this.emitter = this.game.add.emitter(0, 0, 100);
		this.emitter.makeParticles("explosion");

		var prevKey = this.input.keyboard.addKey(Phaser.Keyboard.Q);
		prevKey.onDown.add(this.previousWeapon, this);

		var nextKey = this.input.keyboard.addKey(Phaser.Keyboard.E);
		nextKey.onDown.add(this.nextWeapon, this);

		var resetKey = this.input.keyboard.addKey(Phaser.Keyboard.R);
		resetKey.onDown.add(this.reset, this);

		this.reset();
	},

	nextWeapon: function() {
		this.currentWeapon++;

		if (this.currentWeapon === this.weapons.length) {
			this.currentWeapon = 0;
		}

		this.weaponName.text = this.weapons[this.currentWeapon].name;
	},

	previousWeapon: function() {
		this.currentWeapon--;

		if (this.currentWeapon === -1) {
			this.currentWeapon = this.weapons.length - 1;
		}

		this.weaponName.text = this.weapons[this.currentWeapon].name;
	},

	updateScore: function() {
		var str1 = "Score: ";
		var result = str1.concat(this.score);

		this.scoreText.text = result;
	},

	reset: function() {
		while (this.enemies.children.length != 0) {
			this.enemies.remove(this.enemies.children[0]);
		}

		for (var i = 0; i < 10; i++) {
			var enemy = new PatternEnemy(this.game, this.patterns[0], (this.game.height / 11) * (i + 1));

			var enemySingleBullet = new Weapon.EnemySingleBullet(this.game, "Enemy Single Bullet");

			enemy.attachWeapon(enemySingleBullet);
			this.enemies.add(enemy);
		}
	},

	createExplosion: function(x, y) {
		this.emitter.x = x;
		this.emitter.y = y;

		this.emitter.start(true, 1000, null, 10);
	},

	hit: function(bullet, enemy) {
		this.createExplosion(enemy.x, enemy.y);
		this.enemies.remove(enemy);
		bullet.exists = false;

		this.score += 10;
	},

	playerHit: function(bullet, player) {
		bullet.exists = false;

		this.score -= 100;
	},

	update: function() {
		this.game.physics.arcade.overlap(this.weapons[this.currentWeapon], this.enemies, this.hit, null, this);

		for (var i = 0; i < this.enemies.children.length; i++) {
			this.game.physics.arcade.overlap(this.enemies.children[i].weapon, this.players, this.playerHit, null, this);
		}

		this.player.body.velocity.x = 0;
		this.player.body.velocity.y = 0;

		this.updateScore();

	    if (this.cursors.left.isDown || this.wasd.left.isDown) {
	        this.player.body.velocity.x = -250;
	    }

	   	if (this.cursors.right.isDown || this.wasd.right.isDown) {
	        this.player.body.velocity.x = 250;
	    }

	    if (this.cursors.up.isDown || this.wasd.up.isDown) {
	        this.player.body.velocity.y = -250;
	    }

	    if (this.cursors.down.isDown || this.wasd.down.isDown) {
	        this.player.body.velocity.y = 250;
	    }

	    if (this.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
	    	this.weapons[this.currentWeapon].fire(this.player);
	    }

	    if (this.enemies.children.length === 0) {
	    	this.reset();
	    }

	    for (var i = 0; i < this.enemies.children.length; i++) {
	    	this.enemies.children[i].update();
	    }
	}
}