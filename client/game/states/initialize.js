var initialize = function(game) { }

initialize.prototype = {
	preload: function() {
		var loadingBar = this.add.sprite(160, 240, "loading");
		loadingBar.anchor.setTo(0.5, 0.5);
		this.load.setPreloadSprite(loadingBar);

		this.game.load.image("play", "assets/play.png");
		this.game.load.image('background', 'assets/background.png');
		this.game.load.image("ship", 'assets/ship.png');
		this.game.load.image("alien", 'assets/alien.png');

		this.game.load.image("explosion", 'assets/explosion.png');

		this.game.load.image("bullet1", 'assets/bullet1.png');
		this.game.load.image("bullet2", 'assets/bullet2.png');
		this.game.load.image("bullet3", 'assets/bullet3.png');
		this.game.load.image("bullet4", 'assets/bullet4.png');
		this.game.load.image("bullet5", 'assets/bullet5.png');
		this.game.load.image("bullet6", 'assets/bullet6.png');
		this.game.load.image("bullet7", 'assets/bullet7.png');
		this.game.load.image("bullet8", 'assets/bullet8.png');
		this.game.load.image("bullet9", 'assets/bullet9.png');
		this.game.load.image("bullet10", 'assets/bullet10.png');
		this.game.load.image("bullet11", 'assets/bullet11.png');

		this.game.load.bitmapFont('shmupfont', 'assets/fonts/shmupfont.png', 'assets/fonts/shmupfont.xml');
	},

	create: function() {
		this.game.state.start("MainMenu");
	}
}